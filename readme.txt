﻿====================================
=         mongodb操作              =
====================================

***********************
* 环境配置	      *
***********************

需要下载mongodb ，
linux:
	先创建一个文件夹作为mongodb数据库文件夹如 mongodb\aimlDB
	之后命令行输入
		
		mongod --dbpath mongodb\aimlDB --port 2222
	
	端口号在Session.py初始化时设置为2222，所以在mongod启动的时候也设置为2222


***********************
* mongoDB查询	      *
***********************
命令：（简陋）

连接数据库:
	mongo 127.0.0.1:2222

使用数据库：	
	use test
	（使用数据库test.(默认即为 test)）
查询:	
 	# aiml中存在 history 以及 var 表(mongodb中称为集合collections)	
	# 查询 history 表中的记录:
		
		db.history.find()

	# 条件查询操作，比如查询userID为wz ,botID 为 Alice的对话记录
		
		db.history.find({"userID":"wz","botID":"Alice"})

	# 条件查询操作,大于 小于 等于 不等于的查询
		比如查询userID为wz ,botID 为 Alice, idx 大于5的对话记录
		
		db.history.find({"userID":"wz","botID":"Alice", "idx":{$gt:5}})
		
		大于、小于、等于、不等于、不大于、不小于的符号依次为：
		$gt、$lt、无、$ne、$lte、$gte

删除:
	# 删除全部记录
		
		db.history.remove({})
	
	# 删除所选记录，比如删除userID为wz ,botID 为 Alice的对话记录
		
		db.history.remove({"userID":"wz","botID":"Alice"})
修改:
	# 修改记录，比如修改userID为wz ,botID 为 Alice的记录将botID改为 Bob
	
		db.history.update({"userID":"wz","botID":"Alice"},{$set:{"botID":"Bob"}})


====================================
=         aiml操作                 =
====================================

# 需要先有mongodb支持

***********************
*测试使用过程 	      *
***********************

环境:
	编写和调试过程使用 python anacoda2

运行:
	使用python运行main.py，输入格式为: “输入的话 | 用户ID | 机器人ID”
	如:	hello input|wz|Alice
	机器返回 input: hi
	竖线分割 | 

***********************
*外部调用 aimlwz 时   *
***********************

编写代码：
	# 加载aiml、实例化对象、学习模板

	import aimlwz as aiml
	os.chdir('./alice')
	alice = aiml.Kernel()
	alice.learn("startup.xml") 
	
	# 回到原来目录
	os.chdir('../')
	
	# 调用过程：
	  
	respond = alice.respond(sentence, userID, botID)
	
	"""
	参数类型都为 str:
	sentence:	输入的话。
	userID	:	用户ID
	botID	:	机器人ID
	返回值respond 为返回的对话。。

	"""

**********************************
* 访问mongoDB的详细信息          *
**********************************

访问mongoDB的详细信息的输出 : 在SessionDB.py初始化时由参数version控制。
上一个版本:
	在Kernel.py 第 84 行调用时设置version=True..将其设置为False即可
	在SessionDB.py 第 29、31 行 有调试时的输出。。可将其注释掉。
与使用说明同时更新的版本：
	在SessionDB.py 第 9 行初始化时默认version=True. 将其设置为False即可
	(已在Kernel调用其的时候设置为False)


======================================
=        aiml 其他信息               =
======================================

**********************************
* aiml在 mongodb 中的存储        *
**********************************

因为mongodb未非关系型数据库。不需要提前建表，aiml在mongoDB中存了2类数据。
	1.对话记录
	2.变量记录
存放位置:
	在Session.py中初始化时默认将数据存在mongodb的test数据库中
	对话记录存在 history 表(mongodb中称为集合collections）
	变量记录存在 var 表

记录存储格式:
	1.对话记录: 每条记录(mongodb称为文档files)格式为:
		_id, userID, botID, inputHistory, outputHistory, idx, datetime
	每项解释:
		_id : 为mongodb自带索引(自动生成); 
		userID: 存用户id; botID: 存机器人ID
		inputHistory: 存当前对话的输入
		outputHistory: 存当前对话输出
		idx:	存当前对话为userID与botID对应的第 idx 句话
		datetime: 存存入数据库的时间， 格式为 YYYY-MM-DD hh:mm:ss
	(每次对话时从数据库中取出最近的10条。可在getHistoryDB函数中修改)	
	
	2.变量记录: 每条记录格式为:
		_id, userID, botID, name, value, 2idx, datetime
	每项解释：
		_id : 为mongodb自带索引(自动生成); 
		userID: 存用户id; botID: 存机器人ID
		name: 变量名
		value: 变量值
		2idx: 在userID与botID 对应的第 idx 个对话中存入
		datetime: 存存入数据库的时间， 格式为 YYYY-MM-DD hh:mm:ss
	#（当发生获取变量时, 判断当前时间与变量设置时间是否同个小时，即 YYYY-MM-DD hh 是否相同）
	已改进为变量设置时间与获取时间相差小于 1 小时

**********************************
* aiml对话隔离的体现             *
**********************************

模板 travel.aiml 提供了用于体现aiml对话隔离的例子：

	以简单的旅行购票确定始末地点的情景为例
	
情境中共4轮对话

	输入：我要旅行		# 此处定 topic 为 travel;from与to都为None
	输出：你要去哪

	输入：我要去XXXX	# 此处定 to 为 XXXX，且条件判断如果from非None则直接输出订票。
	输出：从哪出发

	输入：从YYYY		
	输出：从 XXXX 到 YYYY 对吗

	输入：对
	输出：现在定从 XXXX 到 YYYY 的票


同时对3个机器人对话:alice,bob,cain


	输入：我要旅行|wz|alice

	输出：你要去哪

	输入：我要旅行|wz|bob

	输出：你要去哪

	输入：我要旅行|wz|cain

	输出：你要去哪




	输入：我要去北京|wz|alice

	输出：从哪出发

	输入：我要去深圳|wz|bob

	输出：从哪出发



	输入：从哈尔滨|wz|alice

	输出：从 哈 尔 滨 到 北 京 对吗

	输入：我要去广州|wz|cain

	输出：从哪出发


	
	输入：对|wz|alice

	输出：现在定从 哈 尔 滨 到 北 京 的票



====================================
=       custom design操作          =
====================================

# 需要先有mongodb支持

***************************
*外部调用 custom design   *
***************************

编写代码：
	# 加载custom design、实例化对象

	from CustomDesigns import CustomDesign
	cdb = CustomDB()
	

	# 调用过程：
	  
	respond = cdb.match(sentence, userID, botID, collections)
	
	"""
	参数类型都为 str:

		sentence	:	输入的话。
		userID		:	用户ID
		botID		:	机器人ID
		collections	:	mongoDB 集合名

	返回值respond 为符合规则的回复列表:
		形如:
		[{'userID': userID, 'botID': botID, 'ruleID': 0(number), 
		 'RealQuery': sentence, 'method': 'cdb',
		 'Response': response, 'Matched': matched
		},..]

	其中，userID、botID、sentence、response、matched、method 类型为 str
		ruleID 类型为 int

	"""

***************************
*   添加新 rule           *
***************************

编写代码：
	# 加载custom design、实例化对象

	from CustomDesigns import CustomDesign
	cdb = CustomDesign()
	

	# 调用过程：

	#添加单条 rule:

		result = cdb.setRule(collection, userID, botID, match, response)
	
		"""
		参数类型都为 str:
			collection	:	存储集合名
			userID		:	用户ID
			botID		:	机器人ID
			match		:	正则匹配项
			response	:	回复	
		
		返回值表示插入结果 类型为 int， 其中:
			0		:	表示插入数据已存在
			1		:	表示插入成功
			-1		:	表示插入出错 
		"""

	#添加多条 rules:
	
		Tnum, Anum, Snum, Hnum, Enum = cdb.setRuleBatch(collection, userID, botID, ruleList)
		"""
		参数:
			除了 ruleList 类型为 list 其他参数类型都为 str:
			
			collection	:	存储集合名
			userID		:	用户ID
			botID		:	机器人ID
			ruleList	:	规则列表
		
			ruleList 为形如:
				[(matched, response)]
			的list
		
		返回值:
			类型都为 int

			Tnum		:	ruleList 长度
			Anum		:	执行插入的rule数
			Snum		:	插入成功的rule数
			Hnum		:	插入中已存在的rule数
			Enum		:	插入出错的rule数 
		"""

	#从文件中添加多条 rules:
	
		Anum, Snum, Hnum, Enum = cdb.setRuleFromFile(collection, userID, botID, file_name)
		"""
		参数:
			参数类型都为 str:
			
			collection	:	存储集合名
			userID		:	用户ID
			botID		:	机器人ID
			file_name	:	规则文件名
		
			文件 file_name 内的格式:
				文件中每行为一条规则
				格式为：
					matched #@#@#@ response
		
		返回值:
			类型都为 int
			Anum		:	执行插入的rule数
			Snum		:	插入成功的rule数
			Hnum		:	插入中已存在的rule数
			Enum		:	插入出错的rule数

		"""

***************************
*   列举 rule             *
***************************

编写代码：
	# 加载custom design、实例化对象

	from CustomDesigns import CustomDesign
	cdb = CustomDesign()
	

	# 调用过程：

	count, lst = cd.listRule(collection, userID, botID, sort_by, limit, skip)
	
	"""
	参数:
		除了sort_by 类型为list, 其他参数类型都为 str:

		必填参数:
		
		collcetion	:	存储集合名
		
		可选参数:
		
		userID		:	用户ID (默认查询全部)
		botID		:	机器人ID (默认查询全部)
		sort_by		:	排序规则(默认以插入时间与ruleID 顺序)
		limit		:	查询个数(默认不限,考虑之后分页查询)
		skip		:	查询结果中跳过数(默认无,考虑之后分页查询)
	
	返回值:
		count 类型为 int，lst 类型为 list
		count 		:	满足查询的rule数
		lst		:	满足查询的rule的列表
	
			形如:	
	
			[{'userID': userID, 'botID': botID, 'ruleID': 0(number), 
			 'dateime': date,
 			 'matched': matched,
			 'response': response,
			},..]	

		其中，userID、botID、response、matched、datetime类型为 unicode
			ruleID 类型为 int

	"""

====================================
=     custom design 其他信息       =
====================================

************************************
* custom rule在 mongodb 中的存储   *
************************************

设计存在两种规则: 用户定制规则集 user_rule、系统定制规则集 basic_rule
	查询过程将从两种规则集中查询。

存放位置:
	在Session.py中初始化时默认将数据存在mongodb的test数据库中
	用户定制规则 存在 user_rule 集合
	系统定制规则 存在 basic_rule 集合

记录存储格式:
	规则记录: 每条记录(mongodb称为文档files)格式为:
		_id, userID, botID, ruleID, matched, response, datetime
	每项解释:
		_id : 为mongodb自带索引(自动生成); 
		userID: 存用户ID; 
		botID: 存机器人ID
		ruleID: 存规则ID
		matched: 存正则表达式
		response: 存回复
		datetime: 存存入数据库的时间， 格式为 YYYY-MM-DD hh:mm:ss	
	
************************************
* 当前 custom design 存在的不足    *
************************************

对于目前实现的custom design 缺少多轮对话功能
 可能存在添加堆栈，记录历史对话的需求。 

 
====================================
=     Talk Tree 模块使用说明       =
====================================

# 需要先有mongodb支持

***************************
*外部调用 Talk Tree       *
***************************

编写代码：
	# 加载TalkTree、实例化对象

	from TalkTree import TalkTree
	talkTree = TalkTree()
	

	# 调用过程：
	
	# 1.获取当前对话树节点的机器人的话 以及用户选项
	
		sentence, answer = talk.getCurSen(userID, botID, collcetion)
	
		"""
		参数类型都为 str:
		
			userID		:	用户ID
			botID		:	机器人ID
			collections	:	mongoDB 集合名
	
		返回值:
		
			sentence 	:	当前节点机器人的话 类型为 str
			answer		:	当前节点用户选项 类型为 str 的列表
				形如: ["男","女"]
		"""
	
	# 2.匹配用户输入在当前对话树节点的结果
	
		sentence, answer, user_inf = talk.match(query, userID, botID, collcetion, user_inf)
		
			"""
			参数除了 user_inf 为 dict 类型，其他都为 str 类型:
				query		:	用户的输入
				userID		:	用户ID
				botID		:	机器人ID
				collection	:	mongoDB 集合名
				user_inf	:	用户的信息（可省略。用于条件判断，可以从数据库中获取)
		
			返回值:
			
				sentence 	:	当前节点机器人的话 类型为 str。 (若不匹配在返回 None)
				answer		:	当前节点用户选项 类型为 str 的列表	(若不匹配则返回 None)
					形如: ["男","女"]
				user_inf	:	在对话树中处理后的用户信息。目前在树中并无修改以及添加操作
			"""
			
			
====================================
=    解析以及导入 TalkTree         =
====================================

编写代码：

	# 加载 TreeParser、实例化对象

	from TreeParser import TreeParser
	tp = TreeParser()
	
***************************
*从数据库获取一个对话树   *
***************************

	tree = tp.readTreeFromDB(tree_id)
	
	"""
	参数:
	
		tree_id		:	对话树节点ID 类型为int
	
	返回值:
	
		tree 		:	以节点ID为 tree_id 的节点为根节点的对话树 类型为 Node 实例
	"""

***************************
* 将对话树存入数据库      *
***************************

	tree_id = tp.saveIntoDB(tree)
	
	"""
	参数:
	
		tree		:	 对话树 类型为 Node 实例
	
	返回值:
	
		tree_id 	:	tree 在数据库中根节点编号 类型为 int
	"""

***************************
* json 转换成一个对话树   *
***************************

	tree = tp.readTreeFromJSON(json_str)
	
	"""
	参数:
	
		json_str	:	json 格式的字符串
	
	返回值:
	
		tree 		:	依据 json_str 生成的对话树 类型为 Node 实例
	"""
	
***************************
* 将对话树转 json         *
***************************
	
	json_str = tp.saveAsJSON(tree)
		
	"""
	参数:
		
		tree 		:	对话树 类型为 Node 实例
		
	返回值:
		
		json_str	:	json 格式的字符串
	"""
		

***************************
* 根据文件建立对话树      *
***************************

	tree = tp.readFromFile(
		t_file_name,
		s_file_name,
		a_file_name,
		c_file_name
	)
	
	"""
	参数:
	
		t_file_name	:	存树结构的文件
		s_file_name	:	存机器语句的文件
		a_file_name	:	存选项的文件
		c_file_name	:	存条件的文件
	
	返回值:
	
		tree 		:	依据 文件内容 生成的对话树 类型为 Node 实例
	"""
	
***************************
* 将对话树存入文件        *
***************************
	
	tp.saveIntoFile(
		tree,
		t_file_name,
		s_file_name,
		a_file_name,
		c_file_name
	)
		
	"""
	参数:
		
		tree 		:	对话树 类型为 Node 实例
		t_file_name	:	存树结构的文件
		s_file_name	:	存机器语句的文件
		a_file_name	:	存选项的文件
		c_file_name	:	存条件的文件
		
	"""
		
====================================
=       Talk Tree的其他信息        =
====================================

***************************
*  对话树结构             *
***************************

树的每个节点类型为 Tree . 具体内容在 Tree.py
现定节点有2种类型:
	"selection" 与 "condition"
	selecetion 为 用户对话选择节点
	condition 为 条件判断节点
其中，
用户对话选择节点 包括 机器人的话 与 用户可选的回复项
条件判断节点 存储 当前进行判断选择的 条件 与 满足条件时对应的下个节点
	属性:
		_sentence		: 存储当前节点输出的话 ( 对于条件判断节点 该属性无效)
		_conditions		: 存储条件判断节点的子节点条件 ( 对于对话选择节点 该属性无效)
		_children		: 存储其子节点 类型为 dict 
		_tree_id		: 存储当前节点id
		_is_leaf		: 表示当前结点是否是叶节点
		_is_root		: 表示当前节点是否是根节点
		_type			: 节点类型，现包括 条件节点"condition" 与 选择节点"selection" 两类，
							对应不同的处理函数。
		_operation		: 节点操作 (未实际实现。待设计)

		_tmp_user_inf	: 临时存储用户信息，用以条件判断
		
***************************
*  对话树在数据库中的存储 *
***************************

对话树存于 mongoDB 中。共有 4 个表

1.节点表(tree)： 用以存各个节点对应关系
	包含属性：
		PID,SentenceID,	A&N,Root,Leaf,Opration
	各属性说明名:
		PID			:	(int)	节点ID	
		SentenceID	:	(int)	句子ID	
		A&N			:	(dict)	子节点映射, 键为 选项ID 或 条件ID 值为下一个 节点ID
		Root		:	(bool)	是否为根节点
		Leaf		:	(bool)	是否为叶节点
		Type		:	(str)节点类型
		Opration	:	(..)操作(待设计)
		
2.句子表(sentence)： 用以存储机器人的话
	包含属性:
		SentenceID, Content
	各属性说明名:
		SentenceID	:	(int)	句子ID
		Content		:	(str)	句子内容
	
3.选项表(answer)： 用以存储用户可选回答项
	包含属性:
		AnswerID, Content
	各属性说明名:
		AnswerID	:	(int)	选项ID
		Content		:	(str)	选项内容

4.条件表(condition): 用以存储条件内容
	包含属性:
		ConditionID, Content
	各属性说明名:
		ConditionID	:	(int)	条件ID
		Content		:	(list)	条件内容：类型为 列表的列表。
						每个子列表是一个三个元素的列表
							第一个元素为调用的函数名。
							第二个参数是用户参数名。
							第三个参数是默认参数。
						形如:
							[["IS_GT","age",12],[..]..]	
						说明：
							表示调用 IS_GT 函数 用以判断年龄是否大于12
							具体函数在 TreeConditions.py 中实现
							函数映射在 TreeConfig.py 中。
							一个Content的list包括多个 条件函数的调用
						例如：
							[["IS_GT","age",12],["IS_EMPTY","name","True"]]
							则在调用该条件判断时，需要这几个条件同时成立，才会进入下一个节点。



*****************************
*  对话树在文件中的存储     *
*****************************

对话树存在文件中需要4个文件

1.树结构文件:
	内容格式：
	
		(NODE_ID||NODE_TYPE||NODE_SENTENCE_ID||[ NODE_SELECT_ID_1, NODE_SELECT_ID_2]||[ (SUB_NODE_1),(SUB_NODE_2)])

2.机器句子文件:
	内容格式:
	
		ID#@#@#@Content

3.选项文件:
	内容格式:
	
		ID#@#@#@Content

4.条件文件:
	内容格式:
	
		ID#@#@#@Content 

	其中 Content 形如 [[TRUE,TRUE,TRUE],[IS_GT,age,18.0]]

以上几种文件样例依次为:
	trees.txt、sentences.txt、selections.txt、conditions.txt

	此外另有 一个存有 json 形式的对话树，在 tree_json.txt 中

*****************************
*  对话树三种形式的转换问题 *
*****************************

	存在问题：
		因为浅拷贝的原因，
		在Node的实例 tree 被函数调用存入文件后会导致:
			实例 tree 的 conditions 内容由 [[],..,[]] 变成 ['[]','[]']
			在该情况下将 tree 存入数据库后获取将无法正确解析 conditions 内容
		考虑每次存储 tree 之前先将整个 tree 进行深拷贝会有性能影响, 暂时未作处理
		目前建议存储 tree 后不再对其做其他操作。

