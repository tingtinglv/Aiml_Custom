#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/7 17:58
# @Author  : tignting.lv
# @Site    : 
# @File    : views.py
# @Software: PyCharm
#@Contact  : sunfiyes@163.com
# -*- coding:utf-8 -*-
import os
from flask import Flask,render_template,request,redirect,url_for
import sys
import time
from DebugInfo import DebugInfo
from CustomDesigns import CustomDesign
import CustomConfig
from QuestionDB import  Dialog
import Config as cf
from werkzeug.utils import secure_filename
import json
app = Flask(__name__)
# app.config['DEBUG'] = True
# app.debug = True
reload(sys)
sys.setdefaultencoding('utf-8')
flag = True
userId = cf.DEFAULT_ID
ownerId = cf.DEFAULT_ID
botId = cf.DEFAULT_ID
userIdOne = cf.DEFAULT_ID
botIdOne  = cf.DEFAULT_ID
ownerIdOne = cf.DEFAULT_ID
ownerIdMul = cf.DEFAULT_ID
botIdMul = cf.DEFAULT_ID
userIdMul = cf.DEFAULT_ID
cd = CustomDesign(verbose=True)
#-------------------------------------------------custom-----------------------------------------------------
@app.route('/getIdCustom',methods=['POST'])
def getIdCustom():
    global userId
    global ownerId
    global botId
    user = request.form.get("userId")
    bot = request.form.get("botId")
    owner = request.form.get("ownerId")
    if user == "" or user == None:
        user = cf.DEFAULT_ID
    if owner == "" or owner == None:
        owner = cf.DEFAULT_ID
    if bot == "" or bot == None:
        bot = cf.DEFAULT_ID
    userId = user
    ownerId = owner
    botId = bot
    return render_template('custom.html',yx_aiml = None, userId = userId, ownerId = ownerId, botId = botId)

@app.route('/Custom_design/', methods=['POST','GET'])
def Custom_design():
    return render_template('custom_design.html')
@app.route('/Custom_list/', methods=['POST','GET'])
def Custom_list():
    global ownerIdOne
    global userIdOne
    global botIdOne
    customlist = cd.listRule(collection=CustomConfig.custom_designColl, ownerID=ownerIdOne, userID=userIdOne,
                             botID=botIdOne)
    return render_template('custom_list.html',customlist = customlist)
@app.route('/add_oneCustom/', methods=['POST','GET'])
def add_oneCustom():
    global ownerIdOne
    global userIdOne
    global botIdOne
    ownerIdOne = request.form.get("ownerIdOne")
    userIdOne = request.form.get("userIdOne")
    botIdOne = request.form.get("userIdOne")
    matchOne = request.form.get("matchOne")
    requestOne = request.form.get("requestOne")
    result = cd.setSingleRule(CustomConfig.custom_designColl, ownerIdOne, userIdOne, botIdOne, matchOne, requestOne)
    print (result)
    customlist = cd.listRule(collection=CustomConfig.custom_designColl,ownerID=ownerIdOne, userID=userIdOne, botID=botIdOne)
    return render_template("custom_list.html",customlist = customlist)

@app.route('/add_MulCustom', methods=['POST', 'GET'])
def add_MulCustom():
    global userIdMul
    global ownerIdMul
    global botIdMul
    ownerIdMul = request.form.get("ownerIdMul")
    userIdMul = request.form.get("userIdMul")
    botIdMul = request.form.get("botIdMul")
    matchMul = request.form.get("matchMul")
    result = cd.setRuleBatch(collection=CustomConfig.custom_designColl,ownerID = ownerIdMul, userID = userIdMul, botID =botIdMul, ruleList =matchMul)
    print (result)
    customlist = cd.listRule(collection=CustomConfig.custom_designColl, ownerID=ownerIdMul, userID=userIdMul,
                             botID=botIdMul)
    return render_template("custom_list.html", customlist=customlist)

def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['doc', 'file', 'txt'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
@app.route('/add_FileCustom', methods=['POST', 'GET'])
def add_FileCustom():
    global ownerIdFile
    global userIdFile
    global botIdFile
    UPLOAD_FOLDER = "./rules"
    ownerIdFile = request.form.get("ownerIdFile")
    userIdFile = request.form.get("userIdFile")
    botIdFile = request.form.get("botIdFile")
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            os.chdir('./rules')
            filename = secure_filename(file.filename)
            file.save(os.path.join(os.getcwd(), filename))
            cd.setRuleFromFile(collection=CustomConfig.custom_designColl,ownerID=ownerIdFile, userID= userIdFile, botID= botIdFile, file_name=filename)
    customlist = cd.listRule(collection=CustomConfig.custom_designColl, ownerID=ownerIdFile, userID=userIdFile,
                             botID=botIdFile)
    return render_template("custom_list.html", customlist=customlist)

@app.route('/')
def Custom():
    return render_template('custom.html',yx_aiml=None,userId = userId, ownerId = ownerId, botId = botId)

@app.route('/custom_detail',methods=['POST','GET'])
def custom_detail():
    B = DebugInfo()
    cd = CustomDesign(verbose=True)
    global userId, botId, ownerId
    context = request.form.get("context")
    if context == None or context == "":
        return render_template('custom.html', userId = userId, ownerId = ownerId, botId = botId)
    response,debug = cd.match(ownerid= ownerId, userid =userId, botid = botId,timemark= time.asctime(time.localtime(time.time())), moduletype =cf.moduleType, query=context.encode('utf-8'))
    print ("回复为：",response)
    B.addInfo(debug)
    session = Dialog()
    session.insert(cf.dialog_table, ownerId, userId, botId, context.encode('utf-8'), response, cf.CUSTOMTYPE)
    history = session.getDialog(ownerId=ownerId, userId=userId, botId=botId, type=cf.CUSTOMTYPE)
    return render_template('custom.html', history=history, yx_aiml=B.context, userId=userId, ownerId=ownerId,
                           botId=botId)

@app.route('/custom_remote',methods=['POST'])
def custom_remote():
    cd = CustomDesign(verbose=True)
    user = request.form.get("userId")
    bot = request.form.get("botId")
    owner = request.form.get("ownerId")
    userId = user
    ownerId = owner
    botId = bot
    context = request.form.get("context")
    response,debug = cd.match(ownerid= ownerId, userid =userId, botid = botId,timemark= time.asctime(time.localtime(time.time())), moduletype =cf.moduleType, query=context.encode('utf-8'))
    session = Dialog()
    session.insert(cf.dialog_table, ownerId, userId, botId, context.encode('utf-8'), response, cf.CUSTOMTYPE)
    history = session.getDialog(ownerId=ownerId, userId=userId, botId=botId, type=cf.CUSTOMTYPE)
    data = {"debug":debug, "history":history}
    return json.dumps(data)
#------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run(
        host= '0.0.0.0',
        port='5003',
        # debug=True
    )
